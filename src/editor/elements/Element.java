package editor.elements;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;

/**
 * Created by ShadowGorn on 30.10.2015.
 */
public abstract class Element extends mxCell {

    public Element() {
        super();
        setVertex(true);
    }
    public Element(String style,int width, int height, Object value) {
        super(value, new mxGeometry(0, 0, width, height),
                style);
        setVertex(true);
    }
}
