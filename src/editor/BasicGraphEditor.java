package editor;

import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.mxGraphOutline;
import com.mxgraph.swing.util.mxGraphActions;
import com.mxgraph.util.*;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxUndoableEdit.mxUndoableChange;
import com.mxgraph.view.mxGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.List;

public class BasicGraphEditor extends JPanel {
    static {
        try {
            mxResources.add("resources/editor");
        } catch (Exception e) {
            // ignore
        }
    }

    /**
     *
     */
    protected mxGraphComponent graphComponent;

    /**
     *
     */
    protected EditorPalette libraryPane;

    /**
     *
     */
    protected JPanel menu;
    protected JPanel zoom;

    /**
     *
     */
    protected mxUndoManager undoManager;

    /**
     *
     */
    protected String appTitle;

    /**
     *
     */
    protected File currentFile;

    /**
     * Flag indicating whether the current graph has been modified
     */
    protected boolean modified = false;
    /**
     *
     */
    protected mxKeyboardHandler keyboardHandler;

    /**
     *
     */
    protected mxIEventListener undoHandler = new mxIEventListener() {
        public void invoke(Object source, mxEventObject evt) {
            undoManager.undoableEditHappened((mxUndoableEdit) evt
                    .getProperty("edit"));
        }
    };

    /**
     *
     */
    protected mxIEventListener changeTracker = new mxIEventListener() {
        public void invoke(Object source, mxEventObject evt) {
            setModified(true);
        }
    };

    /**
     *
     */
    public BasicGraphEditor(String appTitle, mxGraphComponent component) {
        // Stores and updates the frame title
        this.appTitle = appTitle;

        // Stores a reference to the graph and creates the command history
        graphComponent = component;
        final mxGraph graph = graphComponent.getGraph();
        undoManager = createUndoManager();

        // Do not change the scale and translation after files have been loaded
        graph.setResetViewOnRootChange(false);

        //Disable dangling edges
        graph.setAllowDanglingEdges(false);

        // Updates the modified flag if the graph model changes
        graph.getModel().addListener(mxEvent.CHANGE, changeTracker);

        // Adds the command history to the model and view
        graph.getModel().addListener(mxEvent.UNDO, undoHandler);
        graph.getView().addListener(mxEvent.UNDO, undoHandler);

        // Keeps the selection in sync with the command history
        mxIEventListener undoHandler = new mxIEventListener() {
            public void invoke(Object source, mxEventObject evt) {
                List<mxUndoableChange> changes = ((mxUndoableEdit) evt
                        .getProperty("edit")).getChanges();
                graph.setSelectionCells(graph
                        .getSelectionCellsForChanges(changes));
            }
        };

        undoManager.addListener(mxEvent.UNDO, undoHandler);
        undoManager.addListener(mxEvent.REDO, undoHandler);

        // Creates the library pane that contains the tabs with the palettes
        libraryPane = new EditorPalette();
        libraryPane.setBorder(BorderFactory.createTitledBorder(mxResources.get("Elements")));
        libraryPane.setPreferredSize(new Dimension(130, 400));

        menu = new JPanel(new FlowLayout());
        menu.setBorder(BorderFactory.createTitledBorder(mxResources.get("menu")));
        menu.setBackground(Color.WHITE);

        JButton create = new JButton();
        create.setAction(bind(mxResources.get("new"),
                new EditorActions.NewAction(), "/images/new.gif"));

        JButton open = new JButton();
        open.setAction(bind(mxResources.get("openFile"),
                new EditorActions.OpenAction(), "/images/open.gif"));

        JButton save = new JButton();
        save.setAction(bind(mxResources.get("save"),
                new EditorActions.SaveAction(false), "/images/save.gif"));

        JButton saveas = new JButton();
        saveas.setAction(bind(mxResources.get("saveAs"),
                new EditorActions.SaveAction(true), "/images/saveas.gif"));

        zoom = new JPanel(new BorderLayout());
        zoom.setBackground(Color.WHITE);

        JButton zoomin = new JButton();
        zoomin.setAction(bind("",
                mxGraphActions.getZoomInAction(), "/images/zoom+.gif"));
        zoomin.setContentAreaFilled(false);

        JButton zoomout = new JButton();
        zoomout.setAction(bind("",
                mxGraphActions.getZoomOutAction(), "/images/zoom-.gif"));
        zoomout.setContentAreaFilled(false);

        menu.add(create);
        menu.add(open);
        menu.add(save);
        menu.add(saveas);
        zoom.add(zoomin, BorderLayout.LINE_START);
        zoom.add(zoomout, BorderLayout.LINE_END);

        JPanel btnPanel = new JPanel(new BorderLayout());
        btnPanel.setBackground(Color.WHITE);
        btnPanel.add(menu, BorderLayout.LINE_START);
        btnPanel.add(zoom, BorderLayout.LINE_END);

        // Puts everything together
        setLayout(new BorderLayout());
        add(libraryPane, BorderLayout.BEFORE_LINE_BEGINS);
        add(graphComponent, BorderLayout.CENTER);
        add(btnPanel, BorderLayout.PAGE_START);

        // Installs rubberband selection and handling for some special
        // keystrokes such as F2, Control-C, -V, X, A etc.
        installHandlers();
        installListeners();
        updateTitle();
    }

    /**
     *
     */
    protected mxUndoManager createUndoManager() {
        return new mxUndoManager();
    }

    /**
     *
     */
    protected void installHandlers() {
        keyboardHandler = new EditorKeyboardHandler(graphComponent);
    }

    /**
     *
     */
    public EditorPalette insertPalette(String title) {
        return libraryPane;
    }

    /**
     *
     */
    protected void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0) {
            graphComponent.zoomIn();
        } else {
            graphComponent.zoomOut();
        }
    }


    /**
     *
     */
    protected void showGraphPopupMenu(MouseEvent e) {
        Point pt = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(),
                graphComponent);
        EditorPopupMenu menu = new EditorPopupMenu(BasicGraphEditor.this);
        menu.show(graphComponent, pt.x, pt.y);

        e.consume();
    }

    /**
     *
     */
    protected void installListeners() {
        // Installs mouse wheel listener for zooming
        MouseWheelListener wheelTracker = new MouseWheelListener() {
            /**
             *
             */
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (e.getSource() instanceof mxGraphOutline
                        || e.isControlDown()) {
                    BasicGraphEditor.this.mouseWheelMoved(e);
                }
            }

        };

        graphComponent.addMouseWheelListener(wheelTracker);

        // Installs the popup menu in the graph component
        graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {

            /**
             *
             */
            public void mousePressed(MouseEvent e) {
                // Handles context menu on the Mac where the trigger is on mousepressed
                mouseReleased(e);
            }

            /**
             *
             */
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showGraphPopupMenu(e);
                }
            }

        });
    }

    /**
     *
     */
    public void setCurrentFile(File file) {
        File oldValue = currentFile;
        currentFile = file;

        firePropertyChange("currentFile", oldValue, file);

        if (oldValue != file) {
            updateTitle();
        }
    }

    /**
     *
     */
    public File getCurrentFile() {
        return currentFile;
    }

    /**
     * @param modified
     */
    public void setModified(boolean modified) {
        boolean oldValue = this.modified;
        this.modified = modified;

        firePropertyChange("modified", oldValue, modified);

        if (oldValue != modified) {
            updateTitle();
        }
    }

    /**
     * @return whether or not the current graph has been modified
     */
    public boolean isModified() {
        return modified;
    }

    /**
     *
     */
    public mxGraphComponent getGraphComponent() {
        return graphComponent;
    }

    /**
     *
     */
    public mxUndoManager getUndoManager() {
        return undoManager;
    }

    /**
     * @param name
     * @param action
     * @return a new Action bound to the specified string name
     */
    public Action bind(String name, final Action action) {
        return bind(name, action, null);
    }

    /**
     * @param name
     * @param action
     * @return a new Action bound to the specified string name and icon
     */
    @SuppressWarnings("serial")
    public Action bind(String name, final Action action, String iconUrl) {
        AbstractAction newAction = new AbstractAction(name, (iconUrl != null) ? new ImageIcon(
                BasicGraphEditor.class.getResource(iconUrl)) : null) {
            public void actionPerformed(ActionEvent e) {
                action.actionPerformed(new ActionEvent(getGraphComponent(), e
                        .getID(), e.getActionCommand()));
            }
        };

        newAction.putValue(Action.SHORT_DESCRIPTION, action.getValue(Action.SHORT_DESCRIPTION));

        return newAction;
    }

    /**
     *
     */
    public void updateTitle() {
        JFrame frame = (JFrame) SwingUtilities.windowForComponent(this);

        if (frame != null) {
            String title = (currentFile != null) ? currentFile
                    .getAbsolutePath() : mxResources.get("newDiagram");

            if (modified) {
                title += "*";
            }

            frame.setTitle(title + " - " + appTitle);
        }
    }

    /**
     *
     */
    public void exit() {
        JFrame frame = (JFrame) SwingUtilities.windowForComponent(this);

        if (frame != null) {
            frame.dispose();
        }
    }

    /**
     *
     */
    public JFrame createFrame(JMenuBar menuBar) {
        JFrame frame = new JFrame();
        frame.getContentPane().add(this);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(menuBar);
        frame.setSize(870, 640);

        // Updates the frame title
        updateTitle();

        return frame;
    }
}
