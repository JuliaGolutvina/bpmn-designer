import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import editor.BasicGraphEditor;
import editor.EditorPalette;
import editor.elements.Task;
import org.w3c.dom.Document;

import javax.swing.*;
import java.awt.*;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;

public class GraphEditor extends BasicGraphEditor {

    public GraphEditor() {
        this("mxGraph Editor", new CustomGraphComponent(new CustomGraph()));
    }

    /**
     *
     */
    public GraphEditor(String appTitle, mxGraphComponent component) {
        super(appTitle, component);

        // Creates the shapes palette
        EditorPalette shapesPalette = insertPalette(mxResources.get("shapes"));

        // Adds some template cells for dropping into the graph
        shapesPalette
                .addTemplate(
                        "Container",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/swimlane.png")),
                        "swimlane", 280, 280, "Container");
        shapesPalette
                .addTemplate(
                        "Rectangle",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/rectangle.png")),
                        null, 100, 80, "");
        shapesPalette.addTemplate(
                "Rounded Rectangle",
                new ImageIcon(
                        GraphEditor.class
                                .getResource("/images/rounded.png")),
                new Task("rounded=1", 100, 80, ""));
        shapesPalette
                .addTemplate(
                        "Rhombus",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/rhombus.png")),
                        "rhombus", 80, 80, "");
        shapesPalette
                .addTemplate(
                        "Error",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/error.png")),
                        "roundImage;image=/images/error.png",
                        80, 80, "Error");
        shapesPalette
                .addTemplate(
                        "Fork",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/fork.png")),
                        "rhombusImage;image=/images/fork.png",
                        80, 80, "Fork");
        shapesPalette
                .addTemplate(
                        "Inclusive",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/inclusive.png")),
                        "rhombusImage;image=/images/inclusive.png",
                        80, 80, "Inclusive");
        shapesPalette
                .addTemplate(
                        "Event",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/event.png")),
                        "roundImage;image=/images/event.png",
                        80, 80, "Event");
        shapesPalette
                .addTemplate(
                        "End event",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/event_end.png")),
                        "roundImage;image=/images/event_end.png",
                        80, 80, "End event");
    }

    /**
     *
     */
    public static class CustomGraphComponent extends mxGraphComponent {
        /**
         * @param graph
         */
        public CustomGraphComponent(mxGraph graph) {
            super(graph);

            // Sets switches typically used in an editor
            setPageVisible(false);
            setGridVisible(true);
            setToolTips(true);
            getConnectionHandler().setCreateTarget(false);

            // Loads the defalt stylesheet from an external file
            mxCodec codec = new mxCodec();
            Document doc = mxUtils.loadDocument(GraphEditor.class.getResource(
                    "/resources/default-style.xml")
                    .toString());
            codec.decode(doc.getDocumentElement(), graph.getStylesheet());

            // Sets the background to white
            getViewport().setOpaque(true);
            getViewport().setBackground(Color.WHITE);
        }
    }

    /**
     * A graph that creates new edges from a given template edge.
     */
    public static class CustomGraph extends mxGraph {
        /**
         * Custom graph that defines the alternate edge style to be used when
         * the middle control point of edges is double clicked (flipped).
         */
        public CustomGraph() {
            setAlternateEdgeStyle("edgeStyle=mxEdgeStyle.ElbowConnector;elbow=vertical");
        }

        /**
         * Prints out some useful information about the cell in the tooltip.
         */
        public String getToolTipForCell(Object cell) {
            String tip = "<html>";

            if (getModel().isVertex(cell)) {
                tip += ((mxCell) cell).getValue();
            }

            tip += "</html>";

            return tip;
        }

        /**
         * Overrides the method to use the currently selected edge template for
         * new edges.
         *
         * @param parent
         * @param id
         * @param value
         * @param source
         * @param target
         * @param style
         * @return
         */
        public Object createEdge(Object parent, String id, Object value,
                                 Object source, Object target, String style) {
            return super.createEdge(parent, id, value, source, target, style);
        }

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        mxSwingConstants.SHADOW_COLOR = Color.WHITE;
        mxConstants.W3C_SHADOWCOLOR = "#ffffff";

        GraphEditor editor = new GraphEditor();
        editor.createFrame(new JMenuBar()).setVisible(true);
    }
}
